import discord
from discord.ext import commands
import youtube_dl
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

bot = commands.Bot(command_prefix='!')

# Set up Spotify
spotify = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials())

# Set up youtube_dl
ytdl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
}
ytdl = youtube_dl.YoutubeDL(ytdl_opts)

def play_next(ctx):
    if queue:
        url = queue.pop(0)
        play(ctx, url)
    else:
        voice_client = discord.utils.get(bot.voice_clients, guild=ctx.guild)
        voice_client.disconnect()


def play(ctx, url):
    voice_channel = ctx.author.voice.channel
    voice_client = await voice_channel.connect()
    
    if 'youtube.com' in url:
        with ytdl as ydl:
            info = ydl.extract_info(url, download=False)
            url2 = info['formats'][0]['url']
        voice_client.play(discord.FFmpegPCMAudio(url2)), after=lambda e: play_next(ctx))
        await ctx.send('Now playing from YouTube: ' + url)
    elif 'open.spotify.com' in url:
        track_id = url.split('/')[-1].split('?')[0]
        track = spotify.track(track_id)
        preview_url = track['preview_url']
        voice_client.play(discord.FFmpegPCMAudio(preview_url)), after=lambda e: play_next(ctx))
        await ctx.send('Now playing from Spotify: ' + track['name'])
    else:
        await ctx.send('Unsupported URL')

@bot.command()
async def stop(ctx):
    voice_client = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice_client.is_playing():
        voice_client.stop()
    queue.clear()

@bot.command()
async def pause(ctx):
    voice_client = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice_client.is_playing():
        voice_client.pause()

@bot.command()
async def resume(ctx):
    voice_client = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice_client.is_paused():
        voice_client.resume()

@bot.command()
async def skip(ctx):
    voice_client = discord.utils.get(bot.voice_clients, guild=ctx.guild)
    if voice_client.is_playing():
        voice_client.stop()
